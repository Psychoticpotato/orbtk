use crate::prelude::*;

property!(
    /// `Enabled` describes the enabled / disabled state of a widget.
    Enabled(bool)
);
