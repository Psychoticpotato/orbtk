use crate::prelude::*;

property!(
    /// `Pressed` describes the pressed (touch / mouse) state of a widget.
    Pressed(bool)
);